#!/bin/bash

BASEDIR=$(dirname "$0")

ansible-playbook \
  ${BASEDIR}/../ansible/playbook.yml \
  --inventory ${BASEDIR}/../ansible/inventories/servers/hosts.ini \
  --private-key ${BASEDIR}/../ansible/.secrets/ssh_keys/id_rsa_provisioner \
  --vault-id home@"${BASEDIR}/../ansible/.secrets/vault/servers" \
  --user provisioner \
  $@
