#!/bin/bash

BASEDIR=$(dirname "$0")
ACTION=${1:-encrypt}
echo "started"
for i in $(find ${BASEDIR}/../ansible/inventories/servers -name hosts.ini); do
    echo $i
    ansible-vault \
        ${ACTION} \
        --vault-id home@"${BASEDIR}/../ansible/.secrets/vault/servers" \
        ${i}
done
