# Machine setup with ansible

prerequisite: a `provisioning` user on the host which will be connected to with the key. This user will execute the setup and therefore needs root privileges. The simplest way to create the user is to run (as `sudo su -`    )

```bash
useradd --system --comment="provisioning user" --shell=/bin/bash --groups sudo --create-home provisioner
echo -e "# allow provisioner to sudo without a password\nprovisioner ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/provisioner
```

To simplify the execution of an playbook on a subset of host one should specify the inventory to use in an extra script which runs ansible with the above created user etc. For this repo, those scripts are in `./scripts`

## Steps to create new inventory

can be seen as a new environment which can e.g. be added for a sort of pis, remote servers etc.

1. create a new subdirectory for the new environment; e.g. `localhost`
2. place the hosts in the hosts in the `hosts.ini` file which is located in the new env.
3. add ssh-keys for the hosts in the `.secrets` directory and create key in `vault/<env-name>`
4. create a host-to-role mapping in the `playbook.ini` and crate the roles in `roles/`
5. create a `tasks/main.yml`, triggers in `handlers/main.yml`, templates in `templates/<name>.j2` etc.

## TODOs

make `force: yes` a variable with default `no` to prevent data loss!

### Not started yet

- docker
- software
- add default users
- create provisioner on the fly
- fonts: Fira Mono, Fira Code and SourceCode by Adobe
- conky style

### Needs update

- sync panes in tmux
- zsh spaceship theme
- nvim with `set breakindent` to keep indent on line wrap

